document.getElementById("mainNav").innerHTML = '<a href="index.html">Devi Yip</a><div id="navPages"><a class="mainNav" href="index.html">Work</a><a class="lastNav" href="about.html">About</a></div>';

document.getElementById("loadFooter").innerHTML = '<div class="footer"><p class="center copyright">© Devi Yip 2019</p></div>';


/** w3schools auto hide nav **/
var prevScrollpos = window.pageYOffset;

window.onscroll = function () {

//	console.log("y offset: " + window.pageYOffset);
	if (window.pageYOffset < 100) {
		document.getElementById("navScroll").style.backgroundColor = "transparent";
	} else {
		document.getElementById("navScroll").style.backgroundColor = "var(--bg_color)";
	}
	/*
		console.log(prevScrollpos + " > " + window.pageYOffset);*/
	var currentScrollPos = window.pageYOffset;

	if (prevScrollpos > currentScrollPos ) {
		document.getElementById("navScroll").style.top = "0";
	} else {
		document.getElementById("navScroll").style.top = "-80px";

	}
	prevScrollpos = currentScrollPos;
}

window.onload = function (){
	if (window.pageYOffset < 100) {
		document.getElementById("navScroll").style.backgroundColor = "transparent";
	} else {
		document.getElementById("navScroll").style.backgroundColor = "var(--bg_color)";
	}
}